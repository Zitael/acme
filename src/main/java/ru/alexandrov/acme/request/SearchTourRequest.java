package ru.alexandrov.acme.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class SearchTourRequest {
    private Long countryId;
    private Long hotelId;

    private LocalDate date;
    private Boolean transfer;
    private BigDecimal cost;
    private Integer durationDays;
}
