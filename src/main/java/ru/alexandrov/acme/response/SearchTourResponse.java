package ru.alexandrov.acme.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.alexandrov.acme.dao.entity.Hotel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class SearchTourResponse {
    private Long tourId;

    private Long countryId;
    private String countryName;

    private Long hotelId;
    private String hotelName;

    private LocalDate date;
    private Boolean transfer;
    private BigDecimal cost;
    private Integer durationDays;
}
