package ru.alexandrov.acme.dao.repository;

import ru.alexandrov.acme.dao.TourFilter;

import java.util.List;

public interface CustomTourRepository {
    List findByFilter(TourFilter tourFilter);
}
