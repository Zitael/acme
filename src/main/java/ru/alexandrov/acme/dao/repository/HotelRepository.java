package ru.alexandrov.acme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alexandrov.acme.dao.entity.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, Long> {
}
