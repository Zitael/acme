package ru.alexandrov.acme.dao.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import ru.alexandrov.acme.dao.TourFilter;
import ru.alexandrov.acme.dao.entity.Tour;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CustomTourRepositoryImpl implements CustomTourRepository {
    private final EntityManager em;

    @Override
    public List<Tour> findByFilter(TourFilter filter) {
        Map<String, Object> params = new HashMap<>();

        String select = "select distinct t from Tour t where 1=1 ";

        if (filter.getCountry() != null) {
            select += " and t.hotel.country = :country";
            params.put("country", filter.getCountry());
        } else if (filter.getHotel() != null) {
            select += " and t.hotel = :hotel";
            params.put("hotel", filter.getHotel());
        }

        if (filter.getDate() != null) {
            select += " and t.date = :date";
            params.put("date", filter.getDate());
        }

        if (filter.getTransfer() != null) {
            select += " and t.transfer = :transfer";
            params.put("transfer", filter.getTransfer());
        }

        if (filter.getCost() != null) {
            select += " and t.cost = :cost";
            params.put("cost", filter.getCost());
        }

        if (filter.getDurationDays() != null) {
            select += " and t.durationDays = :durationDays";
            params.put("durationDays", filter.getDurationDays());
        }

        TypedQuery<Tour> query = em.createQuery(select, Tour.class);

        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        return query.getResultList();
    }

}
