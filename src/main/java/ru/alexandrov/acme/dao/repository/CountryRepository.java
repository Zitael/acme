package ru.alexandrov.acme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alexandrov.acme.dao.entity.Country;
import ru.alexandrov.acme.dao.entity.Hotel;

public interface CountryRepository extends JpaRepository<Country, Long> {
}
