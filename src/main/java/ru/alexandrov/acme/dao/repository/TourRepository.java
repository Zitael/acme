package ru.alexandrov.acme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alexandrov.acme.dao.entity.Hotel;
import ru.alexandrov.acme.dao.entity.Tour;

import java.util.List;

public interface TourRepository extends JpaRepository<Tour, Long> {

}
