package ru.alexandrov.acme.dao;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.alexandrov.acme.dao.entity.Country;
import ru.alexandrov.acme.dao.entity.Hotel;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class TourFilter {
    private Hotel hotel;
    private Country country;

    private LocalDate date;
    private Boolean transfer;
    private BigDecimal cost;
    private Integer durationDays;
}
