package ru.alexandrov.acme.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.alexandrov.acme.dao.TourFilter;
import ru.alexandrov.acme.dao.entity.Country;
import ru.alexandrov.acme.dao.entity.Hotel;
import ru.alexandrov.acme.dao.entity.Tour;
import ru.alexandrov.acme.dao.repository.CountryRepository;
import ru.alexandrov.acme.dao.repository.CustomTourRepositoryImpl;
import ru.alexandrov.acme.dao.repository.HotelRepository;
import ru.alexandrov.acme.request.SearchTourRequest;
import ru.alexandrov.acme.response.ErrorResponse;
import ru.alexandrov.acme.response.SearchTourResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PostController {
    private final HotelRepository hotelRepository;
    private final CountryRepository countryRepository;

    private final CustomTourRepositoryImpl customTourRepository;

    @PostMapping("/search-tour")
    public @ResponseBody ResponseEntity<List<SearchTourResponse>> getTour(@RequestBody SearchTourRequest request) {
        TourFilter filter = new TourFilter();

        if (request.getHotelId() != null) {
            filter.setHotel(
                    hotelRepository.findById(request.getHotelId())
                                   .orElseThrow(() -> new RuntimeException("Hotel not found"))
            );
        }

        if (request.getCountryId() != null) {
            filter.setCountry(
                    countryRepository.findById(request.getCountryId())
                                     .orElseThrow(() -> new RuntimeException("Country not found"))
            );
        }

        populate(request, filter);

        List<Tour> tours = customTourRepository.findByFilter(filter);
        if (tours == null || tours.isEmpty()) {
            return notFound().build();
        }

        return ok(
                tours.stream()
                     .map(this::convert)
                     .collect(toList())
        );
    }

    private SearchTourResponse convert(Tour tour) {
        Hotel hotel = tour.getHotel();
        Country country = hotel.getCountry();

        return new SearchTourResponse().setTourId(tour.getId())
                                       .setCountryId(country.getId())
                                       .setCountryName(country.getName())
                                       .setHotelId(hotel.getId())
                                       .setHotelName(hotel.getName())
                                       .setDate(tour.getDate())
                                       .setTransfer(tour.getTransfer())
                                       .setCost(tour.getCost())
                                       .setDurationDays(tour.getDurationDays());
    }

    private void populate(SearchTourRequest request, TourFilter filter) {
        filter.setDate(request.getDate())
              .setTransfer(request.getTransfer())
              .setCost(request.getCost())
              .setDurationDays(request.getDurationDays());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RuntimeException.class)
    public ErrorResponse handleError(RuntimeException ex) {
        return new ErrorResponse(ex.getMessage());
    }
}
