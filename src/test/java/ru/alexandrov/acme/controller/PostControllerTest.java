package ru.alexandrov.acme.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.alexandrov.acme.dao.entity.Country;
import ru.alexandrov.acme.dao.entity.Hotel;
import ru.alexandrov.acme.dao.entity.Tour;
import ru.alexandrov.acme.dao.repository.CountryRepository;
import ru.alexandrov.acme.dao.repository.HotelRepository;
import ru.alexandrov.acme.dao.repository.TourRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

import static java.util.Arrays.asList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PostControllerTest {

    @Autowired MockMvc mockMvc;

    @Autowired HotelRepository hotelRepository;
    @Autowired CountryRepository countryRepository;
    @Autowired TourRepository tourRepository;

    @Before
    public void setUp() {
        Country russia = new Country().setName("Россия");
        Country turkey = new Country().setName("Турция");

        countryRepository.saveAll(asList(russia, turkey));

        Hotel hotel1 = new Hotel().setName("Отель Номер 1")
                                  .setCountry(russia);

        Hotel hotel2 = new Hotel().setName("Отель Номер 2")
                                  .setCountry(turkey);

        Hotel hotel3 = new Hotel().setName("Отель Номер 3")
                                  .setCountry(turkey);

        hotelRepository.saveAll(asList(hotel1, hotel2, hotel3));

        tourRepository.saveAll(asList(
                new Tour().setHotel(hotel1)
                          .setDate(LocalDate.of(2019, 1, 12))
                          .setTransfer(true)
                          .setCost(BigDecimal.valueOf(200))
                          .setDurationDays(8),
                new Tour().setHotel(hotel2)
                          .setDate(LocalDate.of(2019, 2, 1))
                          .setTransfer(true)
                          .setCost(BigDecimal.valueOf(200))
                          .setDurationDays(5),
                new Tour().setHotel(hotel3)
                          .setDate(LocalDate.of(2019, 3, 2))
                          .setTransfer(true)
                          .setCost(BigDecimal.valueOf(200))
                          .setDurationDays(6),
                new Tour().setHotel(hotel1)
                          .setDate(LocalDate.of(2019, 4, 13))
                          .setTransfer(true)
                          .setCost(BigDecimal.valueOf(200))
                          .setDurationDays(7)
        ));
    }

    @Test
    public void getTour_allTours() throws Exception {
        mockMvc.perform(
                post("/search-tour")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
        )
               .andExpect(status().isOk())
               .andExpect(content().json("[\n" +
                       "  {\"tourId\":4,\"countryId\":1,\"countryName\":\"Россия\",\"hotelId\":1,\"hotelName\":\"Отель Номер 1\",\"date\":\"2019-04-13\",\"transfer\":true,\"cost\":200,\"durationDays\":7},\n" +
                       "  {\"tourId\":1,\"countryId\":1,\"countryName\":\"Россия\",\"hotelId\":1,\"hotelName\":\"Отель Номер 1\",\"date\":\"2019-01-12\",\"transfer\":true,\"cost\":200,\"durationDays\":8},\n" +
                       "  {\"tourId\":3,\"countryId\":2,\"countryName\":\"Турция\",\"hotelId\":3,\"hotelName\":\"Отель Номер 3\",\"date\":\"2019-03-02\",\"transfer\":true,\"cost\":200,\"durationDays\":6},\n" +
                       "  {\"tourId\":2,\"countryId\":2,\"countryName\":\"Турция\",\"hotelId\":2,\"hotelName\":\"Отель Номер 2\",\"date\":\"2019-02-01\",\"transfer\":true,\"cost\":200,\"durationDays\":5}\n" +
                       "]"));
    }

    @Test
    public void getTour_hotelNotFound() throws Exception {
        mockMvc.perform(
                post("/search-tour")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"hotelId\": 181\n" +
                                "}")
        )
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{\n" +
                       "  \"error\": \"Hotel not found\"\n" +
                       "}"));
    }

    @Test
    public void getTour_countryNotFound() throws Exception {
        mockMvc.perform(
                post("/search-tour")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"countryId\": 181\n" +
                                "}")
        )
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{\n" +
                       "  \"error\": \"Country not found\"\n" +
                       "}"));
    }

    @Test
    public void getTour_filtered() throws Exception {
        mockMvc.perform(
                post("/search-tour")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"hotelId\": 1,\n" +
                                "  \"date\": \"2019-04-13\",\n" +
                                "  \"transfer\": true,\n" +
                                "  \"cost\": 200,\n" +
                                "  \"durationDays\": 7\n" +
                                "}")
        )
               .andExpect(status().isOk())
               .andExpect(content().json("[\n" +
                       "  {\"tourId\":4,\"countryId\":1,\"countryName\":\"Россия\",\"hotelId\":1,\"hotelName\":\"Отель Номер 1\",\"date\":\"2019-04-13\",\"transfer\":true,\"cost\":200,\"durationDays\":7}\n" +
                       "]"));
    }
}